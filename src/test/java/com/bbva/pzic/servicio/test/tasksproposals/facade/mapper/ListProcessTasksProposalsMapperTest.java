package com.bbva.pzic.servicio.test.tasksproposals.facade.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.servicio.test.tasksproposals.business.dto.InputListProcessTasksProposals;
import com.bbva.pzic.servicio.test.tasksproposals.facade.dto.ProcessTasks;
import com.bbva.pzic.servicio.test.tasksproposals.facade.mapper.impl.ListProcessTasksProposalsMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class ListProcessTasksProposalsMapperTest {

    private IListProcessTasksProposalsMapper mapper;

    @Before
    public void setUp(){
        mapper = new ListProcessTasksProposalsMapper();
    }

    @Test
    public void mapInFullTest() throws IOException {

        InputListProcessTasksProposals request = mapper.mapIn("CAR-432432432", "12345");
        assertNotNull(request);
        assertNotNull(request.getBusinessProcessId());
        assertNotNull(request.getTaskId());
        assertEquals("CAR-432432432", request.getBusinessProcessId());
        assertEquals("12345", request.getTaskId());
    }

    @Test
    public void mapInEmptyTest() throws IOException{
        InputListProcessTasksProposals request = mapper.mapIn(null, null);
        assertNotNull(request);
        assertNull(request.getBusinessProcessId());
        assertNull(request.getTaskId());
    }

    @Test
    public void mapOutFullTest() throws IOException{

        ProcessTasks tasks = new ProcessTasks();
        ServiceResponse<ProcessTasks> response = mapper.mapOut(tasks);
        assertNotNull(response);
        assertNotNull(response.getData());

    }

    @Test
    public void mapOutEmptyTest(){
        ServiceResponse<ProcessTasks> response = mapper.mapOut(null);
        assertNull(response);
    }

}
