package com.bbva.pzic.servicio.test.tasksproposals;

import com.bbva.pzic.servicio.test.tasksproposals.business.dto.InputListProcessTasksProposals;
import com.bbva.pzic.servicio.test.tasksproposals.dao.model.ppcutge1_1.RespuestaTransaccionPpcutge1_1;
import com.bbva.pzic.servicio.test.tasksproposals.util.mappers.ObjectMapperHelper;

import java.io.IOException;

public class EntityMock {

    private static final EntityMock INSTANCE = new EntityMock();
    private ObjectMapperHelper mapperHelper;
    public static EntityMock getInstance(){
        return INSTANCE;
    }
    private EntityMock(){
        mapperHelper = ObjectMapperHelper.getInstance();
    }

    public InputListProcessTasksProposals getInputListProcessTasksProposals() throws IOException {
        return mapperHelper.readValue(
            Thread.currentThread().getContextClassLoader()
                    .getResourceAsStream("mock/InputListProcessTasksProposals.json"), InputListProcessTasksProposals.class
        );
    }

    public RespuestaTransaccionPpcutge1_1 getRespuestaTransaccionPpcutget1_1() throws IOException {
        return mapperHelper.readValue(
                Thread.currentThread().getContextClassLoader()
                        .getResourceAsStream("mock/OutputListProcessTasksProposals.json"), RespuestaTransaccionPpcutge1_1.class
        );
    }
}
