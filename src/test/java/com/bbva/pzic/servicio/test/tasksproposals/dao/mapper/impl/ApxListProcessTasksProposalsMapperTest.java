package com.bbva.pzic.servicio.test.tasksproposals.dao.mapper.impl;


import com.bbva.pzic.servicio.test.tasksproposals.EntityMock;
import com.bbva.pzic.servicio.test.tasksproposals.business.dto.InputListProcessTasksProposals;
import com.bbva.pzic.servicio.test.tasksproposals.dao.apx.mapper.IApxListProcessTasksMapper;
import com.bbva.pzic.servicio.test.tasksproposals.dao.apx.mapper.impl.ApxListProcessTasksMapper;
import com.bbva.pzic.servicio.test.tasksproposals.dao.model.ppcutge1_1.PeticionTransaccionPpcutge1_1;
import com.bbva.pzic.servicio.test.tasksproposals.dao.model.ppcutge1_1.RespuestaTransaccionPpcutge1_1;
import com.bbva.pzic.servicio.test.tasksproposals.facade.dto.ProcessTasks;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InvalidObjectException;

import static org.junit.Assert.*;

public class ApxListProcessTasksProposalsMapperTest {

    private IApxListProcessTasksMapper apxMapper;

    @Before
    public void setUp(){
        apxMapper = new ApxListProcessTasksMapper();
    }

    @Test
    public void mapInFullTest() throws IOException {

        InputListProcessTasksProposals listTasks = EntityMock.getInstance().getInputListProcessTasksProposals();
        PeticionTransaccionPpcutge1_1 request = apxMapper.mapIn(listTasks);
        assertNotNull(request);
        assertNotNull(request.getBusinessprocessid());
        assertNotNull(request.getTaskid());

        assertEquals(listTasks.getBusinessProcessId(), request.getBusinessprocessid());
        assertEquals(listTasks.getTaskId(), request.getTaskid());

    }

    @Test
    public void mapInEmptyTest() throws IOException{
        PeticionTransaccionPpcutge1_1 request = apxMapper.mapIn(new InputListProcessTasksProposals());
        assertNotNull(request);
        assertNull(request.getBusinessprocessid());
        assertNull(request.getTaskid());
    }

    @Test
    public void mapOutFullTest() throws IOException{

        RespuestaTransaccionPpcutge1_1 response = EntityMock.getInstance().getRespuestaTransaccionPpcutget1_1();
        ProcessTasks tasks = apxMapper.mapOut(response);
        assertNotNull(tasks);
        assertNotNull(tasks.getBusinessProcessId());
        assertNotNull(tasks.getTaskId());
        assertNotNull(tasks.getStatus().getId());
        assertNotNull(tasks.getStatus().getDescription());

        assertEquals(response.getCampo_1_businessprocessid(), tasks.getBusinessProcessId());
        assertEquals(response.getCampo_2_taskid(), tasks.getTaskId());
        assertEquals(response.getStatus().getId(), tasks.getStatus().getId());
        assertEquals(response.getStatus().getDescription(), tasks.getStatus().getDescription());

    }

    @Test
    public void mapOutEmptyTest(){
        ProcessTasks task = apxMapper.mapOut(null);
        assertNull(task);
    }
}
