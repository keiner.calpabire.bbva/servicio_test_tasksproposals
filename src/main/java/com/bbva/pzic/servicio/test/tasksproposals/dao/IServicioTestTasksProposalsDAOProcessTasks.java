package com.bbva.pzic.servicio.test.tasksproposals.dao;

import com.bbva.pzic.servicio.test.tasksproposals.business.dto.InputListProcessTasksProposals;
import com.bbva.pzic.servicio.test.tasksproposals.facade.dto.ProcessTasks;

public interface IServicioTestTasksProposalsDAOProcessTasks {

    ProcessTasks listProcessTasksProposals(InputListProcessTasksProposals inputListProcessTasksProposals);

}
