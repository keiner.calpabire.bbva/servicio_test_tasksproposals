package com.bbva.pzic.servicio.test.tasksproposals.facade;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.servicio.test.tasksproposals.facade.dto.ProcessTasks;

public interface ISrvServicioTestTasksProposals {

    ServiceResponse<ProcessTasks> listProcessTasksProposals(String businessProcessId, String taskId);

}
