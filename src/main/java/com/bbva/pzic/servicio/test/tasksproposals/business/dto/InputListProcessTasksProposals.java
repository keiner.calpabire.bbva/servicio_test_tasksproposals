package com.bbva.pzic.servicio.test.tasksproposals.business.dto;


import com.bbva.pzic.servicio.test.tasksproposals.util.ValidationGroup;

import javax.validation.constraints.NotNull;

public class InputListProcessTasksProposals {

    @NotNull(groups = ValidationGroup.ListProcessTasksProposals.class)
    private String businessProcessId;
    private String taskid;

    public String getBusinessProcessId() {
        return businessProcessId;
    }

    public void setBusinessProcessId(String businessProcessId) {
        this.businessProcessId = businessProcessId;
    }

    public String getTaskId() {
        return taskid;
    }

    public void setTaskId(String taskid) {
        this.taskid = taskid;
    }
}
