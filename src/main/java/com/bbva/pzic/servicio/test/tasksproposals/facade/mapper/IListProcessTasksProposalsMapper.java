package com.bbva.pzic.servicio.test.tasksproposals.facade.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.servicio.test.tasksproposals.business.dto.InputListProcessTasksProposals;
import com.bbva.pzic.servicio.test.tasksproposals.facade.dto.ProcessTasks;

public interface IListProcessTasksProposalsMapper {

    InputListProcessTasksProposals mapIn(String businessProcessId, String taskId);

    ServiceResponse<ProcessTasks> mapOut(ProcessTasks processTasks);

}
