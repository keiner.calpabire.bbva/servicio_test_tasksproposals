package com.bbva.pzic.servicio.test.tasksproposals.business.impl;


import com.bbva.pzic.routine.validator.Validator;
import com.bbva.pzic.servicio.test.tasksproposals.business.ISrvIntServicioTestTasksProposals;
import com.bbva.pzic.servicio.test.tasksproposals.business.dto.InputListProcessTasksProposals;
import com.bbva.pzic.servicio.test.tasksproposals.dao.IServicioTestTasksProposalsDAOProcessTasks;
import com.bbva.pzic.servicio.test.tasksproposals.facade.dto.ProcessTasks;
import com.bbva.pzic.servicio.test.tasksproposals.util.ValidationGroup;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SrvIntServicioTestTasksProposals implements ISrvIntServicioTestTasksProposals {

    private static final Log LOG = LogFactory.getLog(SrvIntServicioTestTasksProposals.class);

    @Autowired
    private Validator validator;

    @Autowired
    private IServicioTestTasksProposalsDAOProcessTasks proposalsDAOProcessTasks;

    @Override
    public ProcessTasks listProcessTasksProposals(final InputListProcessTasksProposals listProcessTasksProposals) {
        LOG.info("Estamos en la clase: " + SrvIntServicioTestTasksProposals.class.getName() + " En el metodo: " + "listProcessTasksProposals");
        validator.validate(listProcessTasksProposals, ValidationGroup.ListProcessTasksProposals.class);
        return proposalsDAOProcessTasks.listProcessTasksProposals(listProcessTasksProposals);
    }
}
