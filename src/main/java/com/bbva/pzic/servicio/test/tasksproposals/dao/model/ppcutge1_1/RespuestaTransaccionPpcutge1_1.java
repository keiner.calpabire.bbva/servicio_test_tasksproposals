package com.bbva.pzic.servicio.test.tasksproposals.dao.model.ppcutge1_1;

import com.bbva.jee.arq.spring.core.host.Cabecera;
import com.bbva.jee.arq.spring.core.host.NombreCabecera;
import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.Formato;
import com.bbva.jee.arq.spring.core.host.RespuestaTransaccion;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;



/**
 * Bean de respuesta para la transacci&oacute;n <code>PPCUTGE1</code>
 * 
 * @see PeticionTransaccionPpcutge1_1
 */
@RespuestaTransaccion
@Formato(nombre = "1")
@RooJavaBean
@RooToString
@RooSerializable
public class RespuestaTransaccionPpcutge1_1 {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private static final long serialVersionUID = 1L;
    /**
	 * <p>Cabecera <code>COD-AVISO</code></p>
	 */
	@Cabecera(nombre=NombreCabecera.CODIGO_AVISO)
	private String codigoAviso;
	
	/**
	 * <p>Cabecera <code>DES-AVISO</code></p>
	 */
	@Cabecera(nombre=NombreCabecera.DESCRIPCION_AVISO)
	private String descripcionAviso;
	
	/**
	 * <p>Cabecera <code>COD-UUAA-AVISO</code></p>
	 */
	@Cabecera(nombre=NombreCabecera.APLICACION_AVISO)
	private String aplicacionAviso;
	
	/**
	 * <p>Cabecera <code>COD-RETORNO</code></p>
	 */
	@Cabecera(nombre=NombreCabecera.CODIGO_RETORNO)
	private String codigoRetorno;
	
	/**
	 * <p>Campo <code>campo_1_businessProcessId</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "businessProcessId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 40, signo = true, obligatorio = true)
	private String campo_1_businessprocessid;
	
	/**
	 * <p>Campo <code>campo_2_taskId</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "taskId", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 10, signo = true, obligatorio = true)
	private String campo_2_taskid;
	
	/**
	 * <p>Campo <code>status</code>, &iacute;ndice: <code>3</code>, tipo: <code>DTO</code>
	 */
	@Campo(indice = 3, nombre = "status", tipo = TipoCampo.DTO)
	private Status status;

    /**
     * Sets codigoAviso value
     *
     * @param codigoAviso
     * @return RespuestaTransaccionPpcutge1_1
     */
    public RespuestaTransaccionPpcutge1_1 setCodigoAviso(String codigoAviso) {
        this.codigoAviso = codigoAviso;
        return this;
    }

    /**
     * Gets codigoAviso value
     *
     * @return String
     */
    public String getCodigoAviso() {
        return this.codigoAviso;
    }

    /**
     * Gets descripcionAviso value
     *
     * @return String
     */
    public String getDescripcionAviso() {
        return this.descripcionAviso;
    }

    /**
     * Sets descripcionAviso value
     *
     * @param descripcionAviso
     * @return RespuestaTransaccionPpcutge1_1
     */
    public RespuestaTransaccionPpcutge1_1 setDescripcionAviso(String descripcionAviso) {
        this.descripcionAviso = descripcionAviso;
        return this;
    }

    /**
     * Gets status value
     *
     * @return Status
     */
    public Status getStatus() {
        return this.status;
    }

    /**
     * Sets campo_2_taskid value
     *
     * @param campo_2_taskid
     * @return RespuestaTransaccionPpcutge1_1
     */
    public RespuestaTransaccionPpcutge1_1 setCampo_2_taskid(String campo_2_taskid) {
        this.campo_2_taskid = campo_2_taskid;
        return this;
    }

    /**
     * Gets aplicacionAviso value
     *
     * @return String
     */
    public String getAplicacionAviso() {
        return this.aplicacionAviso;
    }

    /**
     * Sets aplicacionAviso value
     *
     * @param aplicacionAviso
     * @return RespuestaTransaccionPpcutge1_1
     */
    public RespuestaTransaccionPpcutge1_1 setAplicacionAviso(String aplicacionAviso) {
        this.aplicacionAviso = aplicacionAviso;
        return this;
    }

    /**
     * Gets codigoRetorno value
     *
     * @return String
     */
    public String getCodigoRetorno() {
        return this.codigoRetorno;
    }

    /**
     * Sets status value
     *
     * @param status
     * @return RespuestaTransaccionPpcutge1_1
     */
    public RespuestaTransaccionPpcutge1_1 setStatus(Status status) {
        this.status = status;
        return this;
    }

    /**
     * Gets campo_2_taskid value
     *
     * @return String
     */
    public String getCampo_2_taskid() {
        return this.campo_2_taskid;
    }

    /**
     * Sets codigoRetorno value
     *
     * @param codigoRetorno
     * @return RespuestaTransaccionPpcutge1_1
     */
    public RespuestaTransaccionPpcutge1_1 setCodigoRetorno(String codigoRetorno) {
        this.codigoRetorno = codigoRetorno;
        return this;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return String
     */
    public String toString() {
        return "RespuestaTransaccionPpcutge1_1 {" +
                "codigoAviso='" + codigoAviso + '\'' +
                ", descripcionAviso='" + descripcionAviso + '\'' +
                ", aplicacionAviso='" + aplicacionAviso + '\'' +
                ", codigoRetorno='" + codigoRetorno + '\'' +
                ", campo_1_businessprocessid='" + campo_1_businessprocessid + '\'' +
                ", campo_2_taskid='" + campo_2_taskid + '\'' + "}" + super.toString();
    }

    /**
     * Gets campo_1_businessprocessid value
     *
     * @return String
     */
    public String getCampo_1_businessprocessid() {
        return this.campo_1_businessprocessid;
    }

    /**
     * Sets campo_1_businessprocessid value
     *
     * @param campo_1_businessprocessid
     * @return RespuestaTransaccionPpcutge1_1
     */
    public RespuestaTransaccionPpcutge1_1 setCampo_1_businessprocessid(String campo_1_businessprocessid) {
        this.campo_1_businessprocessid = campo_1_businessprocessid;
        return this;
    }
}