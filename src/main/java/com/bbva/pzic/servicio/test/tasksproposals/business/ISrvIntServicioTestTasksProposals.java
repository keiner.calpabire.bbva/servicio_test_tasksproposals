package com.bbva.pzic.servicio.test.tasksproposals.business;

import com.bbva.pzic.servicio.test.tasksproposals.business.dto.InputListProcessTasksProposals;
import com.bbva.pzic.servicio.test.tasksproposals.facade.dto.ProcessTasks;

public interface ISrvIntServicioTestTasksProposals {

    ProcessTasks listProcessTasksProposals(InputListProcessTasksProposals listProcessTasksProposals);

}
