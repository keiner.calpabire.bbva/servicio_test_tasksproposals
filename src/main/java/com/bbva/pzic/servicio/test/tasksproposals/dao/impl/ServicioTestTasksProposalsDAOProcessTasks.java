package com.bbva.pzic.servicio.test.tasksproposals.dao.impl;

import com.bbva.pzic.servicio.test.tasksproposals.business.dto.InputListProcessTasksProposals;
import com.bbva.pzic.servicio.test.tasksproposals.dao.IServicioTestTasksProposalsDAOProcessTasks;
import com.bbva.pzic.servicio.test.tasksproposals.dao.apx.ApxListProcessTasksProposals;
import com.bbva.pzic.servicio.test.tasksproposals.facade.dto.ProcessTasks;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServicioTestTasksProposalsDAOProcessTasks implements IServicioTestTasksProposalsDAOProcessTasks {

    private static final Log LOG = LogFactory.getLog(ServicioTestTasksProposalsDAOProcessTasks.class);

    @Autowired
    private ApxListProcessTasksProposals apx;


    @Override
    public ProcessTasks listProcessTasksProposals(InputListProcessTasksProposals inputListProcessTasksProposals) {

        LOG.info("Estamos en la clase:  " + ServicioTestTasksProposalsDAOProcessTasks.class.getName() + " - Metodo: " + " listProcessTasksProposals");

        return apx.perform(inputListProcessTasksProposals);
    }
}
