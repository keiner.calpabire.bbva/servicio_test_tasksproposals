package com.bbva.pzic.servicio.test.tasksproposals.dao.apx.mapper;

import com.bbva.pzic.servicio.test.tasksproposals.business.dto.InputListProcessTasksProposals;
import com.bbva.pzic.servicio.test.tasksproposals.dao.model.ppcutge1_1.PeticionTransaccionPpcutge1_1;
import com.bbva.pzic.servicio.test.tasksproposals.dao.model.ppcutge1_1.RespuestaTransaccionPpcutge1_1;
import com.bbva.pzic.servicio.test.tasksproposals.facade.dto.ProcessTasks;

public interface IApxListProcessTasksMapper {

    PeticionTransaccionPpcutge1_1 mapIn(InputListProcessTasksProposals input);

    ProcessTasks mapOut(RespuestaTransaccionPpcutge1_1 respuesta);

}
