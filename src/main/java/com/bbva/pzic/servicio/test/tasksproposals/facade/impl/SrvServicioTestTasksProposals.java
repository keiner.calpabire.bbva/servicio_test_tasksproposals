package com.bbva.pzic.servicio.test.tasksproposals.facade.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.jee.arq.spring.core.servicing.annotations.SMC;
import com.bbva.jee.arq.spring.core.servicing.annotations.SN;
import com.bbva.jee.arq.spring.core.servicing.annotations.VN;
import com.bbva.pzic.routine.processing.data.DataProcessingExecutor;
import com.bbva.pzic.servicio.test.tasksproposals.business.impl.SrvIntServicioTestTasksProposals;
import com.bbva.pzic.servicio.test.tasksproposals.facade.ISrvServicioTestTasksProposals;
import com.bbva.pzic.servicio.test.tasksproposals.facade.dto.ProcessTasks;

import com.bbva.pzic.servicio.test.tasksproposals.facade.mapper.IListProcessTasksProposalsMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

@Path("V0")
@Produces(MediaType.APPLICATION_JSON)
@SN(registryID = "SNPE1700055", logicalID = "proposals")
@VN(vnn = "v0")
@Service
public class SrvServicioTestTasksProposals implements ISrvServicioTestTasksProposals {

    private static final Log LOG = LogFactory.getLog(SrvServicioTestTasksProposals.class);

    @Autowired
    private IListProcessTasksProposalsMapper listProcessTasksProposalsMapper;

    @Autowired
    private SrvIntServicioTestTasksProposals servicioTestTasksProposals;

    @Autowired
    private DataProcessingExecutor inputDataProcessingExecutor;

    @Autowired
    private DataProcessingExecutor outputDataProcessingExecutor;


    @Override
    @GET
    @Path("/process-tasks")
    @Consumes(MediaType.APPLICATION_JSON)
    @SMC(registryID = "SMGG20203823" , logicalID = "listProcessTasksProposals" , forcedCatalog = "gabiCatalog")
    public ServiceResponse<ProcessTasks> listProcessTasksProposals(String businessProcessId, String taskId) {

        LOG.info("Estamos en la clase: " + SrvServicioTestTasksProposals.class.getName() + " Metodo GET: " + "listProcessTasksProposals");

        Map<String, Object> queryParams = new HashMap<String, Object>();
        queryParams.put("businessProcessId", businessProcessId);
        queryParams.put("taskId", taskId);

        inputDataProcessingExecutor.perform("SMGG20203823", null, null, queryParams);

        ServiceResponse<ProcessTasks> response = listProcessTasksProposalsMapper.mapOut(
                servicioTestTasksProposals.listProcessTasksProposals(
                        listProcessTasksProposalsMapper.mapIn(
                                (String) queryParams.get("businessProcessId"),
                                (String) queryParams.get("taskId")
                        )
                )
        );

        outputDataProcessingExecutor.perform("SMGG20203823", response, null, null);
        return response;
    }

}
