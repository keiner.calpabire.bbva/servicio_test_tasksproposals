package com.bbva.pzic.servicio.test.tasksproposals.dao.apx.mapper.impl;

import com.bbva.pzic.servicio.test.tasksproposals.business.dto.InputListProcessTasksProposals;
import com.bbva.pzic.servicio.test.tasksproposals.dao.apx.mapper.IApxListProcessTasksMapper;
import com.bbva.pzic.servicio.test.tasksproposals.dao.model.ppcutge1_1.PeticionTransaccionPpcutge1_1;
import com.bbva.pzic.servicio.test.tasksproposals.dao.model.ppcutge1_1.RespuestaTransaccionPpcutge1_1;
import com.bbva.pzic.servicio.test.tasksproposals.facade.dto.ProcessTasks;
import com.bbva.pzic.servicio.test.tasksproposals.facade.dto.Status;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

@Component
public class ApxListProcessTasksMapper implements IApxListProcessTasksMapper {

    private static final Log LOG = LogFactory.getLog(ApxListProcessTasksMapper.class);

    @Override
    public PeticionTransaccionPpcutge1_1 mapIn(InputListProcessTasksProposals input) {
        LOG.info("------------ EN EL METODO MAP IN DEL LA CLASE " + ApxListProcessTasksMapper.class.getName());
        PeticionTransaccionPpcutge1_1 request = new PeticionTransaccionPpcutge1_1();
        request.setBusinessprocessid(input.getBusinessProcessId());
        request.setTaskid(input.getTaskId());
        return request;
    }

    @Override
    public ProcessTasks mapOut(RespuestaTransaccionPpcutge1_1 respuesta) {
        LOG.info("------------ EN EL METODO MAP OUT DEL LA CLASE " + ApxListProcessTasksMapper.class.getName());
        if(respuesta == null){
            return null;
        }
        ProcessTasks tasks = new ProcessTasks();
        tasks.setBusinessProcessId(respuesta.getCampo_1_businessprocessid());
        tasks.setTaskId(respuesta.getCampo_2_taskid());
        tasks.setStatus(convertStatus(respuesta.getStatus()));
        return tasks;
    }

    private Status convertStatus(final com.bbva.pzic.servicio.test.tasksproposals.dao.model.ppcutge1_1.Status status){
        if(status == null){
            return null;
        }
        Status status1 = new Status();
        status1.setDescription(status.getDescription());
        status1.setId(status.getId());
        return status1;
    }
}
