package com.bbva.pzic.servicio.test.tasksproposals.dao.model.ppcutge1_1;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.FilaCampoTabular;
import com.bbva.jee.arq.spring.core.host.TipoCampo;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;

/**
 * <p>Bean fila para el campo tabular <code>status</code>, utilizado por la clase <code>RespuestaTransaccionPpcutge1_1</code></p>
 * 
 * @see RespuestaTransaccionPpcutge1_1
 * 
 * @author Arquitectura Spring BBVA
 */
@FilaCampoTabular
@RooJavaBean
@RooToString
@RooSerializable
public class Status {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private static final long serialVersionUID = 1L;
    /**
	 * <p>Campo <code>id</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "id", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 20, signo = true, obligatorio = true)
	private String id;
	
	/**
	 * <p>Campo <code>description</code>, &iacute;ndice: <code>2</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 2, nombre = "description", tipo = TipoCampo.ALFANUMERICO, longitudMaxima = 100, signo = true, obligatorio = true)
	private String description;

    /**
     * Gets id value
     *
     * @return String
     */
    public String getId() {
        return this.id;
    }

    /**
     * Sets id value
     *
     * @param id
     * @return Status
     */
    public Status setId(String id) {
        this.id = id;
        return this;
    }

    /**
     * Gets description value
     *
     * @return String
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Sets description value
     *
     * @param description
     * @return Status
     */
    public Status setDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return String
     */
    public String toString() {
        return "Status {" +
                "id='" + id + '\'' +
                ", description='" + description + '\'' + "}" + super.toString();
    }
}